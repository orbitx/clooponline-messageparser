#pragma once

#include <thrift/transport/TBufferTransports.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TCompactProtocol.h>
#include <PacketParser.h>
#include <packet_types.h>
#include <mutex>
#include <atomic>
#include "clp_object_pool.h"

class ThriftParser : public PacketParser, public clooponline::packet::Packet
{
   friend class ThriftParserFactory;

   //TODO: constructor should become private so that it can be only created by its factory
   explicit ThriftParser(bool isServer = true): mIsServerFlag(isServer) { }
public:

   ~ThriftParser() override = default;
   std::pair<const unsigned char*, uint> serialize(IBuffer* buffer) override;
   void  deserialize(const unsigned char* payload, uint size) override;
   bool  hasOMagic() override;
   bool  hasEMagic() override;
   ulong getOMagic() override;
   ulong getEMagic() override;
   void  setOMagic(ulong omagic) override;
   void  setEMagic(ulong emagic) override;
   void  eraseOMagic() override;
   void  eraseEMagic() override;
   int   getMessageType() override;
   void  setMessageType(int type) override;
   void  setAsException(std::string text) override;
   void  setAsException(int errorCode) override;
   bool  isException() override;
   std::string getErrorDescription() override;
   void  printTo(std::ostream& out) const override;

private:
   bool mIsServerFlag;
};

class ThriftParserFactory : public PacketParserFactory
{
public:
   explicit ThriftParserFactory(bool isServer)
   //TODO: default object page size should be configurable
      : mIsServer(isServer), OPool(100000)
   { }

   PacketParser* createOne() override
   {
      return new (OPool.createOne())ThriftParser(mIsServer);
   }

private:
   bool mIsServer;
   clp_object_pool<ThriftParser> OPool;
};

inline std::ostream& operator<<(std::ostream& out, const ThriftParser& obj)
{
   obj.printTo(out);
   return out;
}
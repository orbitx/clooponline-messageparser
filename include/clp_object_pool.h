#pragma once

#include "clpbuffer.h"

template <class OBJ>
class clp_object_pool : private clpbuffer
{
public:
   explicit clp_object_pool(unsigned int pool_size)
      : _obj_size(sizeof(OBJ)), clpbuffer(pool_size * sizeof(OBJ))
   { }

   OBJ* createOne()
   {
      return reinterpret_cast<OBJ*>(clpbuffer::reserve(_obj_size));
   }

private:
   unsigned int _obj_size;
};
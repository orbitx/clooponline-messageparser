#include <IBuffer.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TCompactProtocol.h>
#include <boost/make_shared.hpp>

class ThriftBuffer : public IBuffer
{
   friend class ThriftParser;
public:
   ThriftBuffer(uint size = 500)
      : mTBuffer(boost::make_shared<apache::thrift::transport::TMemoryBuffer>(size)),
        mTProtocol(mTBuffer)
   { }

private:
   boost::shared_ptr<apache::thrift::transport::TMemoryBuffer> mTBuffer;
   apache::thrift::protocol::TCompactProtocolT<apache::thrift::transport::TMemoryBuffer> mTProtocol;
};
#pragma once

#include <exception>

class CloopException : public std::exception
{
public:
   CloopException(int errorCode)
      : mErrorCode(errorCode)
   {

   }

   virtual const char* what() { return std::exception::what(); }
   int getCode() { return mErrorCode; }

protected:
   int mErrorCode;
};

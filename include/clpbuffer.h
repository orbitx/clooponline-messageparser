#pragma once

#include <cstring>

#define _likely(x)       __builtin_expect((x),1)
#define _unlikely(x)     __builtin_expect((x),0)

class clpbuffer
{
public:
   explicit clpbuffer(unsigned long size = 1024)
      : mBufferSize(size),
        mBuffer(new unsigned char[mBufferSize]),
        mWriteCursor(mBuffer),
        mBufferEnd(mBuffer + mBufferSize)
   { }

   unsigned char* write(const unsigned char* str, unsigned int length)
   {
      unsigned char* beg = reserve(length);
      if (_likely(beg != nullptr))
         strncpy((char*)beg, (const char*)str, length);
      return beg;
   }

   unsigned char* reserve(unsigned int length)
   {
      if (_unlikely(length > mBufferSize))
         return nullptr;

      mTempReturn = mWriteCursor;
      mWriteCursor += length;
      if (_unlikely(mWriteCursor >= mBufferEnd))
      {
         mWriteCursor = mBuffer + length;
         return mBuffer;
      }
      return mTempReturn;
   }

   ~clpbuffer()
   {
      delete[] mBuffer;
   }

private:
   unsigned long mBufferSize;
   unsigned char* mBuffer;
   unsigned char* mWriteCursor;
   unsigned char* mBufferEnd;
   unsigned char* mTempReturn;
};
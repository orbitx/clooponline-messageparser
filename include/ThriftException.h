#pragma once

#include "Exception.h"
#include <exceptions_types.h>
#include <exceptions_constants.h>

class ThriftException : public CloopException, public clooponline::packet::Exception
{
public:
   ThriftException(clooponline::packet::ExceptionCode::type code)
      : CloopException(code)
   {

   }

   virtual const char* what()
   {
      auto it = clooponline::packet::g_exceptions_constants.EXCEPTION_DESCRIPTION_MAP.find((clooponline::packet::ExceptionCode::type)mErrorCode);
      if (it != clooponline::packet::g_exceptions_constants.EXCEPTION_DESCRIPTION_MAP.end())
         return it->second.c_str();
      else
         return "unknown error";
   }
};
#pragma once

#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <string>
#include <sstream>
#include <exception>
#include <vector>
#include <map>

#define GET_METHOD_TYPE_CHECK(TYPE)                                                                                    \
rapidjson::Value& val = (*mWrappedObject)[attribute];                                                                  \
if(!val.Is##TYPE())                                                                                                    \
   throw std::runtime_error("Type of attribute \"" + std::string(attribute) +                                          \
                            "\" is not \"##TYPE\" in the json document.");                                             \

#define GET_METHOD_BODY(TYPE)                                                                                          \
if(!mWrappedObject->Is##TYPE())                                                                                        \
   throw std::runtime_error("Type of value in json document is not \"##TYPE\"");                                       \
return mWrappedObject->Get##TYPE();

#define GET_MEMBER_CHECK                                                                                               \
if(!mWrappedObject->HasMember(attribute))                                                                              \
   throw std::runtime_error("Attribute \"" + std::string(attribute) + "\" not found in the json document.");           \

#define GET_MEMBER_BODY(TYPE)                                                                                          \
GET_MEMBER_CHECK                                                                                                       \
GET_METHOD_TYPE_CHECK(TYPE)                                                                                            \
return val.Get##TYPE();

#define ARRAY_FILL_LOOP(TYPE)                                                                                          \
for (auto it = val.Begin(); it != val.End(); ++it)                                                                     \
{                                                                                                                      \
   re.push_back(it->Get##TYPE());                                                                                      \
}                                                                                                                      \
                                                                                                                       \
return re;

class JsonParser
{
public:
   typedef rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator>& AllocatorRef;

   JsonParser(rapidjson::Document* wrappedObject, AllocatorRef allocator)
      : mWrappedObject(wrappedObject), mAllocator(allocator), mShouldManageMemory(false),
        mIsDocument(true)
   {
      initialize();
   }

   JsonParser(rapidjson::Value* wrappedObject, AllocatorRef allocator)
      : mWrappedObject(wrappedObject), mAllocator(allocator), mShouldManageMemory(false),
        mIsDocument(false)
   {
      initialize();
   }

   JsonParser(AllocatorRef allocator)
      : mWrappedObject(new rapidjson::Value), mAllocator(allocator), mShouldManageMemory(true),
        mIsDocument(false)
   {
      initialize();
   }

   JsonParser() : mWrappedObject(new rapidjson::Document),
                     mAllocator(static_cast<rapidjson::Document*>(mWrappedObject)->GetAllocator()),
                     mShouldManageMemory(true), mIsDocument(true)
   {
      initialize();
   }

   JsonParser(JsonParser&& rv)
      : mWrappedObject(rv.mWrappedObject), mAllocator(rv.mAllocator), mShouldManageMemory(rv.mShouldManageMemory)
   {
      rv.mShouldManageMemory = false;
      initialize();
   }

   ~JsonParser()
   {
      if (mShouldManageMemory)
      {
         if (mIsDocument)
            delete static_cast<rapidjson::Document*>(mWrappedObject);
         else
            delete mWrappedObject;
      }
   }

   void initialize()
   {   }

   void setObject()
   {
      mWrappedObject->SetObject();
   }

   void setArray()
   {
      mWrappedObject->SetArray();
   }

   const char* serialize()
   {
      mBuffer.Clear();
      rapidjson::Writer<rapidjson::StringBuffer> writer(mBuffer);
      mWrappedObject->Accept(writer);
      return mBuffer.GetString();
   }

   void deserialize(const std::string& document)
   {
      deserialize(document.c_str());
   }

   void deserialize(const char* document)
   {
      rapidjson::Document* doc = static_cast<rapidjson::Document*>(mWrappedObject);
      doc->Parse(document);
      if(doc->HasParseError())
         throw std::runtime_error("wrong json format");
   }

   int getInt()
   {
      GET_METHOD_BODY(Int)
   }
   
   int64_t getInt64()
   {
      GET_METHOD_BODY(Int64)
   }

   uint getUInt()
   {
      GET_METHOD_BODY(Uint)
   }

   uint64_t getUInt64()
   {
      GET_METHOD_BODY(Uint64)
   }

   double getDouble()
   {
      GET_METHOD_BODY(Double)
   }

   bool getBool()
   {
      GET_METHOD_BODY(Bool)
   }

   const char* getString()
   {
      GET_METHOD_BODY(String)
   }

   int getIntMember(const char* attribute)
   {
      GET_MEMBER_BODY(Int)
   }

   int64_t getInt64Member(const char* attribute)
   {
      GET_MEMBER_BODY(Int64)
   }

   uint getUintMember(const char* attribute)
   {
      GET_MEMBER_BODY(Uint)
   }

   uint64_t getUint64Member(const char* attribute)
   {
      GET_MEMBER_BODY(Uint64)
   }

   double getDoubleMember(const char* attribute)
   {
      GET_MEMBER_BODY(Double)
   }

   bool getBoolMember(const char* attribute)
   {
      GET_MEMBER_BODY(Bool)
   }
   
   const char* getStringMember(const char* attribute)
   {
      GET_MEMBER_BODY(String)
   }

   JsonParser getObjectMember(const char* attribute)
   {
      GET_MEMBER_CHECK
      GET_METHOD_TYPE_CHECK(Object)
      return JsonParser(&val, mAllocator);
   }

   std::vector<int> getIntArrayMember(const char* attribute)
   {
      GET_MEMBER_CHECK
      GET_METHOD_TYPE_CHECK(Array)
      std::vector<int> re;
      ARRAY_FILL_LOOP(Int)
   }

   std::vector<int64_t> getInt64ArrayMember(const char* attribute)
   {
      GET_MEMBER_CHECK
      GET_METHOD_TYPE_CHECK(Array)
      std::vector<int64_t> re;
      ARRAY_FILL_LOOP(Int64)
   }

   std::vector<uint> getUintArrayMember(const char* attribute)
   {
      GET_MEMBER_CHECK
      GET_METHOD_TYPE_CHECK(Array)
      std::vector<uint> re;
      ARRAY_FILL_LOOP(Uint)
   }

   std::vector<uint64_t> getUint64ArrayMember(const char* attribute)
   {
      GET_MEMBER_CHECK
      GET_METHOD_TYPE_CHECK(Array)
      std::vector<uint64_t> re;
      ARRAY_FILL_LOOP(Uint64)
   }

   std::vector<double> getDoubleArrayMember(const char* attribute)
   {
      GET_MEMBER_CHECK
      GET_METHOD_TYPE_CHECK(Array)
      std::vector<double> re;
      ARRAY_FILL_LOOP(Double)
   }

   std::vector<bool> getBoolArrayMember(const char* attribute)
   {
      GET_MEMBER_CHECK
      GET_METHOD_TYPE_CHECK(Array)
      std::vector<bool> re;
      ARRAY_FILL_LOOP(Bool)
   }

   std::vector<const char*> getStringArrayMember(const char* attribute)
   {
      GET_MEMBER_CHECK
      GET_METHOD_TYPE_CHECK(Array)
      std::vector<const char*> re;
      ARRAY_FILL_LOOP(String)
   }

   std::vector<JsonParser> getObjectArrayMember(const char* attribute)
   {
      GET_MEMBER_CHECK
      GET_METHOD_TYPE_CHECK(Array)
      std::vector<JsonParser> re;
      for (auto it = val.Begin(); it != val.End(); ++it)
      {
         re.push_back(JsonParser(it, mAllocator));
      }

      return re;
   }

   std::map<std::string, JsonParser> getAllElements()
   {
      std::map<std::string, JsonParser> re;
      for (auto it = mWrappedObject->MemberBegin(); it != mWrappedObject->MemberEnd(); ++it)
      {
         re.insert(std::make_pair(std::string(it->name.GetString()), JsonParser(&(it->value), mAllocator)));
      }

      return re;
   }

   template <class T>
   void setValue(T value)
   {
      mWrappedObject->Set(value, mAllocator);
   }

   template <class T>
   JsonParser& addMember(const char* attName, T value)
   {
      rapidjson::Value nameval(rapidjson::StringRef(attName));
      rapidjson::Value valval(value);
      mWrappedObject->AddMember(nameval, valval, mAllocator);
      return *this;
   }

   JsonParser& addMember(const char* attName, const char* value)
   {
      rapidjson::Value nameval(rapidjson::StringRef(attName));
      rapidjson::Value valval(rapidjson::StringRef(value));
      mWrappedObject->AddMember(nameval, valval, mAllocator);
      return *this;
   }

   JsonParser& addMember(const char* attName, const JsonParser& value)
   {
      rapidjson::Value nameval(rapidjson::StringRef(attName));
      mWrappedObject->AddMember(nameval, *(value.mWrappedObject), mAllocator);
      return *this;
   }


   template <class T>
   JsonParser& addMember(const char* attName, const std::vector<T>& value)
   {
      rapidjson::Value nameval(rapidjson::StringRef(attName));
      rapidjson::Value valarr;
      valarr.SetArray();

      for (auto v : value)
      {
         valarr.PushBack(rapidjson::Value(v), mAllocator);
      }

      mWrappedObject->AddMember(nameval, valarr, mAllocator);
      return *this;
   }

   JsonParser& addMember(const char* attName, const std::vector<const char*>& value)
   {
      rapidjson::Value nameval(rapidjson::StringRef(attName));
      rapidjson::Value valarr;
      valarr.SetArray();

      for (auto& v : value)
      {
         valarr.PushBack(rapidjson::Value(rapidjson::StringRef(v)), mAllocator);
      }

      mWrappedObject->AddMember(nameval, valarr, mAllocator);
      return *this;
   }

   JsonParser& addMember(const char* attName, const std::vector<JsonParser*>& value)
   {
      rapidjson::Value nameval(rapidjson::StringRef(attName));
      rapidjson::Value valarr;
      valarr.SetArray();

      for (auto v : value)
      {
         valarr.PushBack(*(v->mWrappedObject), mAllocator);
      }

      mWrappedObject->AddMember(nameval, valarr, mAllocator);
      return *this;
   }

   bool isArray()
   {
      return mWrappedObject->IsArray();
   }

   bool isObject()
   {
      return mWrappedObject->IsObject();
   }

   bool isValue()
   {
      return !isArray() && !isObject();
   }

   bool hasMember(const char* attribute)
   {
      return mWrappedObject->HasMember(attribute);
   }

   void eraseMember(const char* attribute)
   {
      if (!hasMember(attribute))
         throw std::runtime_error("Attribute \"" + std::string(attribute) + "\" not found in the json document.");

      mWrappedObject->EraseMember(attribute);
   }

   template <class T>
   void push_back(T value)
   {
      mWrappedObject->PushBack(rapidjson::Value(value), mAllocator);
   }

   void push_back(const char* value)
   {
      mWrappedObject->PushBack(rapidjson::Value(rapidjson::StringRef(value)), mAllocator);
   }

   void push_back(const JsonParser& value)
   {
      mWrappedObject->PushBack(*(value.mWrappedObject), mAllocator);
   }

   const AllocatorRef getAllocator() { return mAllocator; }

private:
   rapidjson::Value* mWrappedObject;
   bool              mShouldManageMemory;
   bool              mIsDocument;
   AllocatorRef      mAllocator;
   rapidjson::StringBuffer mBuffer;

};

template void JsonParser::setValue<int>(int value);
template void JsonParser::setValue<int64_t>(int64_t value);
template void JsonParser::setValue<uint>(uint value);
template void JsonParser::setValue<uint64_t>(uint64_t value);
template void JsonParser::setValue<bool>(bool value);

template JsonParser& JsonParser::addMember<int>(const char* attName, int value);
template JsonParser& JsonParser::addMember<int64_t>(const char* attName, int64_t value);
template JsonParser& JsonParser::addMember<uint>(const char* attName, uint value);
template JsonParser& JsonParser::addMember<uint64_t>(const char* attName, uint64_t value);
template JsonParser& JsonParser::addMember<bool>(const char* attName, bool value);

template JsonParser& JsonParser::addMember<int>(const char* attName, const std::vector<int>& value);
template JsonParser& JsonParser::addMember<int64_t>(const char* attName, const std::vector<int64_t>& value);
template JsonParser& JsonParser::addMember<uint>(const char* attName, const std::vector<uint>& value);
template JsonParser& JsonParser::addMember<uint64_t>(const char* attName, const std::vector<uint64_t>& value);
template JsonParser& JsonParser::addMember<bool>(const char* attName, const std::vector<bool>& value);

template void JsonParser::push_back<int>(int value);
template void JsonParser::push_back<int64_t>(int64_t value);
template void JsonParser::push_back<uint>(uint value);
template void JsonParser::push_back<uint64_t>(uint64_t value);
template void JsonParser::push_back<bool>(bool value);

#pragma once

#include <boost/asio/detail/shared_ptr.hpp>
#include <IBuffer.h>

class PacketParser
{
public:
   virtual ~PacketParser() = default;

   virtual std::pair<const unsigned char*, uint> serialize(IBuffer* buffer) = 0;
   virtual void  deserialize(const unsigned char* payload, uint size) = 0;
   virtual bool  hasOMagic() = 0;
   virtual bool  hasEMagic() = 0;
   virtual ulong getOMagic() = 0;
   virtual ulong getEMagic() = 0;
   virtual void  setOMagic(ulong omagic) = 0;
   virtual void  setEMagic(ulong emagic) = 0;
   virtual void  eraseOMagic() = 0;
   virtual void  eraseEMagic() = 0;
   virtual int   getMessageType() = 0;
   virtual void  setMessageType(int type) = 0;
   virtual void  setAsException(std::string text) = 0;
   virtual void  setAsException(int errorCode) = 0;
   virtual bool  isException() = 0;
   virtual std::string getErrorDescription() = 0;
   virtual void  printTo(std::ostream& stream) const = 0;
};

class PacketParserFactory
{
public:
   virtual PacketParser* createOne() = 0;
};

extern std::ostream& operator<<(std::ostream& out, const PacketParser& obj);
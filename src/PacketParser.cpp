#include <PacketParser.h>

std::ostream& operator<<(std::ostream& out, const PacketParser& obj)
{
   obj.printTo(out);
   return out;
}
#include "ThriftParser.h"
#include <thrift/TToString.h>
#include <ThriftException.h>
#include <boost/make_shared.hpp>
#include <ThriftBuffer.h>

using namespace std;
using apache::thrift::transport::TMemoryBuffer;
using apache::thrift::protocol::TCompactProtocol;
using apache::thrift::protocol::TCompactProtocolT;
using namespace clooponline::packet;

std::pair<const unsigned char*, uint> ThriftParser::serialize(IBuffer* buffer)
{
   auto tbuffer = (ThriftBuffer*)buffer;
   tbuffer->mTBuffer->resetBuffer();
   this->write(&tbuffer->mTProtocol);
   unsigned char* bptr;
   uint32_t rlen;
   tbuffer->mTBuffer->getBuffer(&bptr, &rlen);
   return {bptr, rlen};
}

void ThriftParser::deserialize(const unsigned char* payload, uint size)
{
   auto buff = boost::make_shared<TMemoryBuffer>((uint8_t*)payload, size);
   auto prot = TCompactProtocolT<TMemoryBuffer>(buff);
   this->read(&prot);
}

bool ThriftParser::hasOMagic()
{
   return (mIsServerFlag) ? __isset.smagic : __isset.cmagic;
}

bool ThriftParser::hasEMagic()
{
   return (mIsServerFlag) ? __isset.cmagic : __isset.smagic;
}

ulong ThriftParser::getOMagic()
{
   return (mIsServerFlag) ? (ulong)smagic : (ulong)cmagic;
}

ulong ThriftParser::getEMagic()
{
   return (mIsServerFlag) ? (ulong)cmagic : (ulong)smagic;
}

void ThriftParser::setOMagic(ulong omagic)
{
   (mIsServerFlag) ? __set_smagic((int64_t)omagic) :  __set_cmagic((int64_t)omagic);
}

void ThriftParser::setEMagic(ulong emagic)
{
   (mIsServerFlag) ? __set_cmagic((int64_t)emagic) :  __set_smagic((int64_t)emagic);
}

void ThriftParser::eraseOMagic()
{
   ((mIsServerFlag) ? __isset.smagic :  __isset.cmagic) = false;
}

void ThriftParser::eraseEMagic()
{
   ((mIsServerFlag) ? __isset.cmagic :  __isset.smagic) = false;
}

int ThriftParser::getMessageType()
{
   return msgType;
}

void ThriftParser::setMessageType(int type)
{
   __set_msgType((PacketType::type)type);
   switch ((PacketType::type)type)
   {
      case PacketType::IDENTIFY:
         __isset.pIdentify = true;
         break;
      case PacketType::CREATE_SESSION:
         __isset.pCreateSession = true;
         break;
      case PacketType::CREATE_SESSION_RESPONSE:
         __isset.pCreateSessionResponse = true;
         break;
      case PacketType::JOIN_SESSION:
         __isset.pJoinSession = true;
         break;
      case PacketType::JOIN_SESSION_RESPONSE:
         __isset.pJoinSessionResponse = true;
         break;
      case PacketType::JOINED:
         __isset.pJoined = true;
         break;
      case PacketType::SUBSCRIBE:
         __isset.pSubscribe = true;
         break;
      case PacketType::SUBSCRIBE_RESPONSE:
         __isset.pSubscribeResponse = true;
         break;
      case PacketType::SUBSCRIPTION_RESULT:
         __isset.pSubscriptionResult = true;
         break;
      case PacketType::UNSUBSCRIBE:
         __isset.pUnsubscribe = true;
         break;
      case PacketType::CHANGE_SIDE:
         __isset.pChangeSide = true;
         break;
      case PacketType::SIDE_CHANGED:
         __isset.pSideChanged = true;
         break;
      case PacketType::CHANGE_CONFIGS:
         __isset.pChangeConfigs = true;
         break;
      case PacketType::CONFIG_CHANGED:
         __isset.pConfigsChanged = true;
         break;
      case PacketType::LEAVE_SESSION:
         __isset.pLeaveSession = true;
         break;
      case PacketType::TEMP_LEAVE:
         __isset.pTemporaryLeave = true;
         break;
      case PacketType::TEMP_LEFT:
         __isset.pTemporaryLeft = true;
         break;
      case PacketType::SESSION_LIST:
         __isset.pSessionList = true;
         break;
      case PacketType::SESSION_LIST_RESPONES:
         __isset.pSessionListResponse = true;
         break;
      case PacketType::START:
         __isset.pStart = true;
         break;
      case PacketType::READY:
         __isset.pReady = true;
         break;
      case PacketType::SET_READY:
         __isset.pSetReady = true;
         break;
      case PacketType::CANCEL_START:
         __isset.pCancelStart = true;
         break;
      case PacketType::START_CANCELED:
         __isset.pStartCanceled = true;
         break;
      case PacketType::STARTED:
         __isset.pStarted = true;
         break;
      case PacketType::BE_MANAGER:
         __isset.pBeManager = true;
         break;
      case PacketType::RPC_CALL:
         __isset.pRPCCall = true;
         break;
      case PacketType::RPC_CALL_RESPONSE:
         __isset.pRPCCallResponse = true;
         break;
      case PacketType::RPC_CALLED:
         __isset.pRPCCalled = true;
         break;
      case PacketType::SET_PROPERTIES:
         __isset.pSetProperties = true;
         break;
      case PacketType::SET_PROPERTIES_RESPONSE:
         __isset.pSetPropertiesResponse = true;
         break;
      case PacketType::PROPERTIES_CHANGED:
         __isset.pPropertiesChanged = true;
         break;
      case PacketType::GET_PROPERTIES:
         __isset.pGetProperties = true;
         break;
      case PacketType::GET_PROPERTIES_RESPONSE:
         __isset.pGetPropertiesResponse = true;
         break;
      case PacketType::MANAGER_KICK:
         __isset.pManagerKick = true;
         break;
      case PacketType::LEFT:
         __isset.pLeft = true;
         break;
      case PacketType::GET_AVAILABLE_ACTIVE_SESSIONS:
         __isset.pGetActiveSessions = true;
         break;
      case PacketType::AVAILABLE_ACTIVE_SESSIONS:
         __isset.pActiveSessions = true;
         break;
      case PacketType::HANDLE_ACTIVE_SESSION:
         __isset.pHandleActiveSession = true;
         break;
      case PacketType::HANDLE_ACTIVE_SESSION_RESPONSE:
         __isset.pHandleActiveSessionResponse = true;
         break;
      case PacketType::REJOINED:
         __isset.pRejoined = true;
         break;
      case PacketType::LOCKED:
         __isset.pLocked = true;
         break;
      case PacketType::RESUMED:
         __isset.pResumed = true;
         break;
      case PacketType::TERMINATE:
         __isset.pTerminate = true;
         break;
      case PacketType::TERMINATED:
         __isset.pTerminated = true;
         break;
      case PacketType::EXCEPTION:
         __isset.pException = true;
         break;
   }
}

void ThriftParser::setAsException(std::string text)
{
   setMessageType(PacketType::EXCEPTION);
   pException.__set_errorCode(ExceptionCode::UNEXPECTED_EXCEPTION);
   pException.__set_description(text);
}

void ThriftParser::setAsException(int errorCode)
{
   setMessageType(PacketType::EXCEPTION);
   pException.__set_errorCode((ExceptionCode::type)errorCode);
}

bool ThriftParser::isException()
{
   return msgType == PacketType::EXCEPTION;
}

std::string ThriftParser::getErrorDescription()
{
   if (!isException())
      return "no error";
   return ThriftException (pException.errorCode).what();
}

void ThriftParser::printTo(std::ostream& out) const
{
   using ::apache::thrift::to_string;
   out << "Packet(";
   out << "msgType=" << to_string(msgType);
   out << ", " << "smagic="; (__isset.smagic ? (out << to_string(smagic)) : (out << "<null>"));
   out << ", " << "cmagic="; (__isset.cmagic ? (out << to_string(cmagic)) : (out << "<null>"));

   switch (msgType)
   {
      case PacketType::IDENTIFY:
         out << ", " << "pIdentify="; (__isset.pIdentify ? (out << to_string(pIdentify)) : (out << "<null>"));
         break;
      case PacketType::CREATE_SESSION:
         out << ", " << "pCreateSession="; (__isset.pCreateSession ? (out << to_string(pCreateSession)) : (out << "<null>"));
         break;
      case PacketType::CREATE_SESSION_RESPONSE:
         out << ", " << "pCreateSessionResponse="; (__isset.pCreateSessionResponse ? (out << to_string(pCreateSessionResponse)) : (out << "<null>"));
         break;
      case PacketType::JOIN_SESSION:
         out << ", " << "pJoinSession="; (__isset.pJoinSession ? (out << to_string(pJoinSession)) : (out << "<null>"));
         break;
      case PacketType::JOIN_SESSION_RESPONSE:
         out << ", " << "pJoinSessionResponse="; (__isset.pJoinSessionResponse ? (out << to_string(pJoinSessionResponse)) : (out << "<null>"));
         break;
      case PacketType::JOINED:
         out << ", " << "pJoined="; (__isset.pJoined ? (out << to_string(pJoined)) : (out << "<null>"));
         break;
      case PacketType::SUBSCRIBE:
         out << ", " << "pSubscribe="; (__isset.pSubscribe ? (out << to_string(pSubscribe)) : (out << "<null>"));
         break;
      case PacketType::SUBSCRIBE_RESPONSE:
         out << ", " << "pSubscribeResponse="; (__isset.pSubscribeResponse ? (out << to_string(pSubscribeResponse)) : (out << "<null>"));
         break;
      case PacketType::SUBSCRIPTION_RESULT:
         out << ", " << "pSubscriptionResult="; (__isset.pSubscriptionResult ? (out << to_string(pSubscriptionResult)) : (out << "<null>"));
         break;
      case PacketType::UNSUBSCRIBE:
         out << ", " << "pUnsubscribe="; (__isset.pUnsubscribe ? (out << to_string(pUnsubscribe)) : (out << "<null>"));
         break;
      case PacketType::CHANGE_SIDE:
         out << ", " << "pChangeSide="; (__isset.pChangeSide ? (out << to_string(pChangeSide)) : (out << "<null>"));
         break;
      case PacketType::SIDE_CHANGED:
         out << ", " << "pSideChanged="; (__isset.pSideChanged ? (out << to_string(pSideChanged)) : (out << "<null>"));
         break;
      case PacketType::CHANGE_CONFIGS:
         out << ", " << "pChangeConfigs="; (__isset.pChangeConfigs ? (out << to_string(pChangeConfigs)) : (out << "<null>"));
         break;
      case PacketType::CONFIG_CHANGED:
         out << ", " << "pConfigsChanged="; (__isset.pConfigsChanged? (out << to_string(pConfigsChanged)) : (out << "<null>"));
         break;
      case PacketType::LEAVE_SESSION:
         out << ", " << "pLeaveSession="; (__isset.pLeaveSession? (out << to_string(pLeaveSession)) : (out << "<null>"));
         break;
      case PacketType::TEMP_LEAVE:
         out << ", " << "pTemporaryLeave="; (__isset.pTemporaryLeave ? (out << to_string(pTemporaryLeave)) : (out << "<null>"));
         break;
      case PacketType::TEMP_LEFT:
         out << ", " << "pTemporaryLeft="; (__isset.pTemporaryLeft ? (out << to_string(pTemporaryLeft)) : (out << "<null>"));
         break;
      case PacketType::SESSION_LIST:
         out << ", " << "pSessionList="; (__isset.pSessionList ? (out << to_string(pSessionList)) : (out << "<null>"));
         break;
      case PacketType::SESSION_LIST_RESPONES:
         out << ", " << "pSessionListResponse="; (__isset.pSessionListResponse ? (out << to_string(pSessionListResponse)) : (out << "<null>"));
         break;
      case PacketType::START:
         out << ", " << "pStart="; (__isset.pStart ? (out << to_string(pStart)) : (out << "<null>"));
         break;
      case PacketType::READY:
         out << ", " << "pReady="; (__isset.pReady ? (out << to_string(pReady)) : (out << "<null>"));
         break;
      case PacketType::SET_READY:
         out << ", " << "pSetReady="; (__isset.pSetReady ? (out << to_string(pSetReady)) : (out << "<null>"));
         break;
      case PacketType::CANCEL_START:
         out << ", " << "pCancelStart="; (__isset.pCancelStart ? (out << to_string(pCancelStart)) : (out << "<null>"));
         break;
      case PacketType::START_CANCELED:
         out << ", " << "pStartCanceled="; (__isset.pStartCanceled ? (out << to_string(pStartCanceled)) : (out << "<null>"));
         break;
      case PacketType::STARTED:
         out << ", " << "pStarted="; (__isset.pStarted ? (out << to_string(pStarted)) : (out << "<null>"));
         break;
      case PacketType::BE_MANAGER:
         out << ", " << "pBeManager="; (__isset.pBeManager ? (out << to_string(pBeManager)) : (out << "<null>"));
         break;
      case PacketType::RPC_CALL:
         out << ", " << "pRPCCall="; (__isset.pRPCCall ? (out << to_string(pRPCCall)) : (out << "<null>"));
         break;
      case PacketType::RPC_CALL_RESPONSE:
         out << ", " << "pRPCCallResponse="; (__isset.pRPCCallResponse ? (out << to_string(pRPCCallResponse)) : (out << "<null>"));
         break;
      case PacketType::RPC_CALLED:
         out << ", " << "pRPCCalled="; (__isset.pRPCCalled ? (out << to_string(pRPCCalled)) : (out << "<null>"));
         break;
      case PacketType::SET_PROPERTIES:
         out << ", " << "pSetProperties="; (__isset.pSetProperties ? (out << to_string(pSetProperties)) : (out << "<null>"));
         break;
      case PacketType::SET_PROPERTIES_RESPONSE:
         out << ", " << "pSetPropertiesResponse="; (__isset.pSetPropertiesResponse ? (out << to_string(pSetPropertiesResponse)) : (out << "<null>"));
         break;
      case PacketType::PROPERTIES_CHANGED:
         out << ", " << "pPropertiesChanged="; (__isset.pPropertiesChanged ? (out << to_string(pPropertiesChanged)) : (out << "<null>"));
         break;
      case PacketType::GET_PROPERTIES:
         out << ", " << "pGetProperties="; (__isset.pGetProperties ? (out << to_string(pGetProperties)) : (out << "<null>"));
         break;
      case PacketType::GET_PROPERTIES_RESPONSE:
         out << ", " << "pGetPropertiesResponse="; (__isset.pGetPropertiesResponse ? (out << to_string(pGetPropertiesResponse)) : (out << "<null>"));
         break;
      case PacketType::MANAGER_KICK:
         out << ", " << "pManagerKick="; (__isset.pManagerKick ? (out << to_string(pManagerKick)) : (out << "<null>"));
         break;
      case PacketType::LEFT:
         out << ", " << "pLeft="; (__isset.pLeft ? (out << to_string(pLeft)) : (out << "<null>"));
         break;
      case PacketType::GET_AVAILABLE_ACTIVE_SESSIONS:
         out << ", " << "pGetActiveSessions="; (__isset.pGetActiveSessions ? (out << to_string(pGetActiveSessions)) : (out << "<null>"));
         break;
      case PacketType::AVAILABLE_ACTIVE_SESSIONS:
         out << ", " << "pActiveSessions="; (__isset.pActiveSessions ? (out << to_string(pActiveSessions)) : (out << "<null>"));
         break;
      case PacketType::HANDLE_ACTIVE_SESSION:
         out << ", " << "pHandleActiveSession="; (__isset.pHandleActiveSession ? (out << to_string(pHandleActiveSession)) : (out << "<null>"));
         break;
      case PacketType::HANDLE_ACTIVE_SESSION_RESPONSE:
         out << ", " << "pHandleActiveSessionResponse="; (__isset.pHandleActiveSessionResponse ? (out << to_string(pHandleActiveSessionResponse)) : (out << "<null>"));
         break;
      case PacketType::REJOINED:
         out << ", " << "pRejoined="; (__isset.pRejoined ? (out << to_string(pRejoined)) : (out << "<null>"));
         break;
      case PacketType::LOCKED:
         out << ", " << "pLocked="; (__isset.pLocked ? (out << to_string(pLocked)) : (out << "<null>"));
         break;
      case PacketType::RESUMED:
         out << ", " << "pResumed="; (__isset.pResumed ? (out << to_string(pResumed)) : (out << "<null>"));
         break;
      case PacketType::TERMINATE:
         out << ", " << "pTerminate="; (__isset.pTerminate ? (out << to_string(pTerminate)) : (out << "<null>"));
         break;
      case PacketType::TERMINATED:
         out << ", " << "pTerminated="; (__isset.pTerminated ? (out << to_string(pTerminated)) : (out << "<null>"));
         break;
      case PacketType::EXCEPTION:
         out << ", " << "pException="; (__isset.pException ? (out << to_string(pException)) : (out << "<null>"));
         break;
   }

   out << ")";
}
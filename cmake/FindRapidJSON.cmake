# set search hint directories
set(
     RapidJSON_POSSIBLE_ROOT_PATHS
     $ENV{RapidJSON_ROOT}
     /usr/local
     /usr
   )


# find RapidJSON include directory
# =================================================================================

find_path(
  RapidJSON_INCLUDE_DIR
  NAME          RapidJSON/RapidJSON.h
  NAME          rapidjson/rapidjson.h
  HINTS         ${RapidJSON_POSSIBLE_ROOT_PATHS}
)

if(NOT RapidJSON_INCLUDE_DIR)
  message(STATUS "Checking for RapidJSON... no")
  message(STATUS "Could not find include path for RapidJSON, try setting RapidJSON_ROOT")
  return()
endif()


# everything is found. just finish up
# =================================================================================

set(RapidJSON_FOUND TRUE CACHE BOOL "Whether RapidJSON is found on the system or not")
set(RapidJSON_INCLUDE_DIR ${RapidJSON_INCLUDE_DIR} CACHE PATH "RapidJSON include directory")

message(STATUS "Checking for RapidJSON... yes")
